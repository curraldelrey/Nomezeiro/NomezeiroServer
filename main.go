package main

import (
	"log"
	"os"
	"time"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/crypto"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/daemon"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/database"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/settings"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/signalhandler"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/route"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("You must provide at least an argument.")
		os.Exit(1)
	}

	switch os.Args[1] {
	case "server":
		runDaemon()
	default:
		runCommands()
	}
}

func init() {
	log.Println("Initializing")
}

func loadModules() {
	var err error
	if err = logging.Initialize(); err != nil {
		log.Panic("Critical error: could not load the logging module successfully. Error: " + err.Error())
		panic("Fatal error during initialization.")
	}
	if err = settings.Initialize(); err != nil {
		logging.Critical("Critical error: could not load the settings module successfully. Error: " + err.Error())
		panic("Fatal error during initialization.")
	}
	if err = database.Initialize(); err != nil {
		logging.Critical("Critical error: could not load the database module successfully. Error: " + err.Error())
		panic("Fatal error during initialization.")
	}
	if err = signalhandler.Initialize(); err != nil {
		logging.Critical("Critical error: could not load the signalhandler module successfully. Error: " + err.Error())
		panic("Fatal error during initialization.")
	}
	if err = crypto.Initialize(); err != nil {
		logging.Critical("Critical error: could not load the crypto module successfully. Error: " + err.Error())
		panic("Fatal error during initialization.")
	}
	if err = route.Initialize(); err != nil {
		logging.Critical("Critical error: could not load the route module successfully. Error: " + err.Error())
		panic("Fatal error during initialization.")
	}
}

func runDaemon() {

	loadModules()
	logging.Info("Main modules initialized")

	go daemon.Initialize()
	go route.StartServer()
	go close()
	for {
		time.Sleep(5 * time.Second)
		logging.Trace("Working...")
	}
}

func runCommands() {
	daemon.RunCommand(os.Args[1:])
}

func close() {
	<-signalhandler.Close
	logging.Info("Terminating modules.")
	daemon.Close()
	logging.Info("Daemon closed.")
	database.Close()
	logging.Info("Database closed.")
	logging.Info("Program's clean-ish, the rest can solve itself.")
	os.Exit(0)
}
