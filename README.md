# NomezeiroServer
### Um servidor incrivelmente desnecessário feito em Go.

---

Projetos de ciência e trabalhos de escola estão ficando cada vez mais complexos, e a última fronteira é romper com o paradigma de que existe limite para essa zoeira.

Então, vem a ideia de criar uma nova versão do Nomezeiro com uma completa arquitetura cliente-servidor-banco. Será que vai deslanchar?

---

## O que vai ter potencialmente?

- Conexão com MySQL para CRUD de nomes
- Um sistema de clientes
- Mano, isso não pode ser sério

---

## Licença

WTFPL 2.0