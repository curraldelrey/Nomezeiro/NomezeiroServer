package route

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"github.com/dvsekhvalnov/jose2go"
	"github.com/gorilla/context"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/model"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/crypto"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

func loggingMW(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logging.Trace(r.Method + " request to " + r.URL.Path + " received.")
		next(w, r)
	}
}

func authMW(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//Alright. So here we check for the authorization header first.
		authorizationHeader := r.Header.Get("authorization")
		//If we have something there, it might be the token. Split and parse.
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")

			if len(bearerToken) == 2 {
				payload, _, err := jose.Decode(bearerToken[1], crypto.GetPublicKey())
				if err != nil {
					json.NewEncoder(w).Encode(model.APIError{Error: err.Error(), Code: http.StatusTeapot})
					return
				}
				//the payload could be decoded. Time to verify it.
				logging.Trace("Payload: " + payload)
				var u model.User

				json.Unmarshal([]byte(payload), &u)

				//Timestamp check. If it's closed to expiration, the user should grab a new one.

				if u.Timestamp <= (time.Now().Unix() - 30) {
					json.NewEncoder(w).Encode(model.APIError{Error: "Expired authorization token", Code: http.StatusExpectationFailed})
					return
				}

				context.Set(r, "user", u)
				next(w, r)
			} else {
				json.NewEncoder(w).Encode(model.APIError{Error: "Invalid authorization token", Code: http.StatusUnauthorized})
			}
		} else {
			json.NewEncoder(w).Encode(model.APIError{Error: "An authorization header is required", Code: http.StatusUnauthorized})
		}
	})
}
