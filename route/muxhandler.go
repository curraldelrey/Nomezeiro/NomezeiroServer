package route

import (
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/controller"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var (
	//router contains the multiplexer object.
	router *mux.Router
)

var routes = Routes{
	Route{
		"Auth",
		"POST",
		"/auth",
		loggingMW(controller.AuthPOST),
	},
	Route{
		"GetFirstnames",
		"GET",
		"/firstnames",
		loggingMW(authMW(controller.FirstnamesGET)),
	},
	Route{
		"AddFirstname",
		"POST",
		"/firstnames",
		loggingMW(authMW(controller.FirstnamesPOST)),
	},
	Route{
		"EditFirstname",
		"PUT",
		"/firstnames",
		loggingMW(authMW(controller.FirstnamesPUT)),
	},
	Route{
		"DeleteFirstname",
		"DELETE",
		"/firstnames",
		loggingMW(authMW(controller.FirstnamesDELETE)),
	},
	Route{
		"GetMiddlenames",
		"GET",
		"/middlenames",
		loggingMW(authMW(controller.MiddlenamesGET)),
	},
	Route{
		"AddMiddlename",
		"POST",
		"/middlenames",
		loggingMW(authMW(controller.MiddlenamesPOST)),
	},
	Route{
		"EditMiddlename",
		"PUT",
		"/middlenames",
		loggingMW(authMW(controller.MiddlenamesPUT)),
	},
	Route{
		"DeleteMiddlename",
		"DELETE",
		"/middlenames",
		loggingMW(authMW(controller.MiddlenamesDELETE)),
	},
	Route{
		"GetLastnames",
		"GET",
		"/lastnames",
		loggingMW(authMW(controller.LastnamesGET)),
	},
	Route{
		"AddLastname",
		"POST",
		"/lastnames",
		loggingMW(authMW(controller.LastnamesPOST)),
	},
	Route{
		"EditLastname",
		"PUT",
		"/lastnames",
		loggingMW(authMW(controller.LastnamesPUT)),
	},
	Route{
		"DeleteLastname",
		"DELETE",
		"/lastnames",
		loggingMW(authMW(controller.LastnamesDELETE)),
	},
	Route{
		"GetAreacodes",
		"GET",
		"/areacodes",
		loggingMW(authMW(controller.AreacodesGET)),
	},
	Route{
		"GetPhonenumbers",
		"GET",
		"/phonenumbers",
		loggingMW(authMW(controller.PhonenumbersGET)),
	},
	Route{
		"AddPhonenumber",
		"POST",
		"/phonenumbers",
		loggingMW(authMW(controller.PhonenumbersPOST)),
	},
	Route{
		"EditPhonenumber",
		"PUT",
		"/phonenumbers",
		loggingMW(authMW(controller.PhonenumbersPUT)),
	},
	Route{
		"DeletePhonenumber",
		"DELETE",
		"/phonenumbers",
		loggingMW(authMW(controller.PhonenumbersDELETE)),
	},
	Route{
		"GetIntegrations",
		"GET",
		"/integrations",
		loggingMW(authMW(controller.IntegrationsGET)),
	},
	Route{
		"AddIntegration",
		"POST",
		"/integrations",
		loggingMW(authMW(controller.IntegrationsPOST)),
	},
	Route{
		"EditIntegration",
		"PUT",
		"/integrations",
		loggingMW(authMW(controller.IntegrationsPUT)),
	},
	Route{
		"DeleteIntegration",
		"DELETE",
		"/integrations",
		loggingMW(authMW(controller.IntegrationsDELETE)),
	},
	Route{
		"GetZipcodes",
		"GET",
		"/zipcodes",
		loggingMW(authMW(controller.ZipcodesGET)),
	},
	Route{
		"AddZipcode",
		"POST",
		"/zipcodes",
		loggingMW(authMW(controller.ZipcodesPOST)),
	},
	Route{
		"EditZipcode",
		"PUT",
		"/zipcodes",
		loggingMW(authMW(controller.ZipcodesPUT)),
	},
	Route{
		"DeleteZipcode",
		"DELETE",
		"/zipcodes",
		loggingMW(authMW(controller.ZipcodesDELETE)),
	},
	Route{
		"GetExtrafields",
		"GET",
		"/extrafields",
		loggingMW(authMW(controller.ExtrafieldsGET)),
	},
	Route{
		"AddExtrafield",
		"POST",
		"/extrafields",
		loggingMW(authMW(controller.ExtrafieldsPOST)),
	},
	Route{
		"EditExtrafield",
		"PUT",
		"/extrafields",
		loggingMW(authMW(controller.ExtrafieldsPUT)),
	},
	Route{
		"DeleteExtrafield",
		"DELETE",
		"/extrafields",
		loggingMW(authMW(controller.ExtrafieldsDELETE)),
	},
}

//Initialize starts off the multiplexer for the REST entrypoints.
func Initialize() error {
	logging.Trace("Initializing http router.")
	router = mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler
		handler = route.HandlerFunc

		router.Methods(route.Method).Path(route.Pattern).
			Name(route.Name).Handler(handler)
	}

	http.Handle("/", router)

	return nil
}

//StartServer loads the main mux loop.
func StartServer() {
	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "POST", "DELETE", "PUT"})
	err := http.ListenAndServe(":"+"42123",
		handlers.CORS(allowedOrigins, allowedMethods)(router))
	if err != nil {
		logging.Critical("Multiplexer error: " + err.Error())
		os.Exit(1)
	}
}
