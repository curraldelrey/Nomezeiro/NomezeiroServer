package controller

import (
	"encoding/json"
	"net/http"
	"reflect"

	"github.com/gorilla/context"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/model"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/database"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

func defaultHandler(w *http.ResponseWriter, r *http.Request, t model.DBType, dbFunc database.DBFunc) {
	ctx := context.Get(r, "user")
	u := ctx.(model.User)

	err := json.NewDecoder(r.Body).Decode(t)
	if err != nil {
		logging.Critical("Could not decode the " + r.Method + " request: " + err.Error())
		json.NewEncoder(*w).Encode(model.APIError{Error: "Invalid JSON", Code: http.StatusBadRequest})
		return
	}

	id := int(reflect.Indirect(reflect.ValueOf(t)).FieldByName("UserID").Int())
	if u.UserID != id {
		json.NewEncoder(*w).Encode(model.APIError{Error: "Invalid request - ID does not correspond to the token credentials", Code: http.StatusTeapot})
		return
	}
	err = dbFunc(t)
	if err != nil {
		json.NewEncoder(*w).Encode(model.APIError{Error: err.Error(), Code: http.StatusUnprocessableEntity})
		return
	}
	json.NewEncoder(*w).Encode(t)
}
