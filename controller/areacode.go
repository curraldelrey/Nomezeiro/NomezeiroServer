package controller

import (
	"encoding/json"
	"net/http"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/model"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/database"
)

//AreacodesGET handles requests for all Areacodes available.
func AreacodesGET(w http.ResponseWriter, r *http.Request) {
	res, err := database.FindAreacodes(&model.Areacode{""})
	if err != nil {
		json.NewEncoder(w).Encode(model.APIError{Error: err.Error(), Code: http.StatusExpectationFailed})
		return
	}
	json.NewEncoder(w).Encode(res)
}
