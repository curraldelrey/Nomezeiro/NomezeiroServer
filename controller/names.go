package controller

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/context"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/model"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/database"
)

//FirstnamesGET handles requests for all Firstnames available to an user.
func FirstnamesGET(w http.ResponseWriter, r *http.Request) {
	u := context.Get(r, "user").(model.User)
	res, _ := database.FindFirstnames(&model.Firstname{0, "", u.UserID})
	json.NewEncoder(w).Encode(res)
}

//FirstnamesPOST inserts a new Firstname.
func FirstnamesPOST(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewFirstname(), database.DefaultInsert)
}

//FirstnamesPUT edits a Firstname.
func FirstnamesPUT(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewFirstname(), database.DefaultUpdate)
}

//FirstnamesDELETE deletes a Firstname.
func FirstnamesDELETE(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewFirstname(), database.DefaultDelete)
}

//MiddlenamesGET handles requests for all Middlenames available to an user.
func MiddlenamesGET(w http.ResponseWriter, r *http.Request) {
	u := context.Get(r, "user").(model.User)
	res, _ := database.FindMiddlenames(&model.Middlename{0, "", u.UserID})
	json.NewEncoder(w).Encode(res)
}

//MiddlenamesPOST inserts a new Middlename.
func MiddlenamesPOST(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewMiddlename(), database.DefaultInsert)
}

//MiddlenamesPUT edits a Middlename.
func MiddlenamesPUT(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewMiddlename(), database.DefaultUpdate)
}

//MiddlenamesDELETE deletes a Middlename.
func MiddlenamesDELETE(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewMiddlename(), database.DefaultDelete)
}

//LastnamesGET handles requests for all Lastnames available to an user.
func LastnamesGET(w http.ResponseWriter, r *http.Request) {
	u := context.Get(r, "user").(model.User)
	res, _ := database.FindLastnames(&model.Lastname{0, "", u.UserID})
	json.NewEncoder(w).Encode(res)
}

//LastnamesPOST inserts a new Lastname.
func LastnamesPOST(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewLastname(), database.DefaultInsert)
}

//LastnamesPUT edits a Lastname.
func LastnamesPUT(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewLastname(), database.DefaultUpdate)
}

//LastnamesDELETE deletes a Lastname.
func LastnamesDELETE(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewLastname(), database.DefaultDelete)
}
