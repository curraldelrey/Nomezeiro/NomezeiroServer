package controller

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/dvsekhvalnov/jose2go"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/model"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/crypto"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/database"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

//AuthPOST handles authentication requests.
func AuthPOST(w http.ResponseWriter, r *http.Request) {
	var cred model.Credentials
	err := json.NewDecoder(r.Body).Decode(&cred)
	if err != nil {
		logging.Critical("Could not decode the auth request: " + err.Error())
		json.NewEncoder(w).Encode(model.APIError{Error: "Invalid JSON", Code: http.StatusBadRequest})
		return
	}

	user, err := database.FindUser(cred.Username)
	if err != nil {
		json.NewEncoder(w).Encode(model.APIError{Error: "User not found", Code: http.StatusUnauthorized})
		return
	}

	hash, _ := database.GetUserPassword(user.UserID)
	if !crypto.CompareHash(hash, []byte(cred.Password)) {
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(model.APIError{Error: "Bad credentials", Code: http.StatusUnauthorized})
		return
	}

	user.Timestamp = time.Now().Unix() + 3600

	payload, _ := json.Marshal(user)
	token, err := jose.Sign(string(payload), jose.RS512, crypto.GetPrivateKey())

	json.NewEncoder(w).Encode(model.JwtToken{Token: token, User: *user})
}
