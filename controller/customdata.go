package controller

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/context"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/model"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/database"
)

//PhonenumbersGET handles requests for all Phonenumbers available to an user.
func PhonenumbersGET(w http.ResponseWriter, r *http.Request) {
	u := context.Get(r, "user").(model.User)
	res, _ := database.FindPhonenumbers(&model.Phonenumber{0, "", u.UserID})
	json.NewEncoder(w).Encode(res)
}

//PhonenumbersPOST inserts a new Phonenumber.
func PhonenumbersPOST(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewPhonenumber(), database.DefaultInsert)
}

//PhonenumbersPUT edits a Phonenumber.
func PhonenumbersPUT(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewPhonenumber(), database.DefaultUpdate)
}

//PhonenumbersDELETE deletes a Phonenumber.
func PhonenumbersDELETE(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewPhonenumber(), database.DefaultDelete)
}

//IntegrationsGET handles requests for all Integrations available to an user.
func IntegrationsGET(w http.ResponseWriter, r *http.Request) {
	u := context.Get(r, "user").(model.User)
	res, _ := database.FindIntegrations(&model.Integration{0, "", u.UserID})
	json.NewEncoder(w).Encode(res)
}

//IntegrationsPOST inserts a new Integrations.
func IntegrationsPOST(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewIntegration(), database.DefaultInsert)
}

//IntegrationsPUT edits an Integration.
func IntegrationsPUT(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewIntegration(), database.DefaultUpdate)
}

//IntegrationsDELETE deletes an Integration.
func IntegrationsDELETE(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewIntegration(), database.DefaultDelete)
}

//ZipcodesGET handles requests for all Zipcodes available to an user.
func ZipcodesGET(w http.ResponseWriter, r *http.Request) {
	u := context.Get(r, "user").(model.User)
	res, _ := database.FindZipcodes(&model.Zipcode{0, "", u.UserID})
	json.NewEncoder(w).Encode(res)
}

//ZipcodesPOST inserts a new Zipcode.
func ZipcodesPOST(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewZipcode(), database.DefaultInsert)
}

//ZipcodesPUT edits a Zipcode.
func ZipcodesPUT(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewZipcode(), database.DefaultUpdate)
}

//ZipcodesDELETE deletes a Zipcode.
func ZipcodesDELETE(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewZipcode(), database.DefaultDelete)
}

//ExtrafieldsGET handles requests for all Extrafields available to an user.
func ExtrafieldsGET(w http.ResponseWriter, r *http.Request) {
	u := context.Get(r, "user").(model.User)
	res, _ := database.FindExtrafields(&model.Extrafield{0, "", "", u.UserID})
	json.NewEncoder(w).Encode(res)
}

//ExtrafieldsPOST inserts a new Extrafield.
func ExtrafieldsPOST(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewExtrafield(), database.DefaultInsert)
}

//ExtrafieldsPUT edits an Extrafield.
func ExtrafieldsPUT(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewExtrafield(), database.DefaultUpdate)
}

//ExtrafieldsDELETE deletes an Extrafield.
func ExtrafieldsDELETE(w http.ResponseWriter, r *http.Request) {
	defaultHandler(&w, r, model.NewExtrafield(), database.DefaultDelete)
}
