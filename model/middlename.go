package model

//Middlenames contain a list om them.
type Middlenames struct {
	Middlenames []Middlename `json:"middlenames"`
}

//Middlename exposes its string and corresponding owner.
type Middlename struct {
	MiddlenameID int    `json:"middlename_id"`
	Middlename   string `json:"middlename"`
	UserID       int    `json:"user_id"`
}

//NewMiddlename constructs an otherwise empty Middlename type pointer.
func NewMiddlename() *Middlename {
	m := Middlename{0, "", 0}
	return &m
}

//NewMiddlenameSlice constructs an otherwise empty Middlename slice pointer.
func NewMiddlenameSlice() *Middlenames {
	m := Middlenames{}
	return &m
}

func (n Middlename) DBSelect() string {
	return "SELECT MIDDLENAME_ID, MIDDLENAME, USER_ID FROM MIDDLENAME WHERE USER_ID IN (?, ?)"
}
func (n Middlename) DBInsert() string {
	return "INSERT INTO MIDDLENAME (MIDDLENAME, USER_ID) VALUES (?, ?)"
}
func (n Middlename) DBUpdate() string {
	return "UPDATE MIDDLENAME SET MIDDLENAME = ? WHERE MIDDLENAME_ID = ? AND USER_ID = ?"
}
func (n Middlename) DBDelete() string {
	return "DELETE FROM MIDDLENAME WHERE MIDDLENAME_ID = ?"
}

func (n Middlename) DBQueryArgs() []interface{} {
	return []interface{}{1, n.UserID}
}
func (n Middlename) DBInsertArgs() []interface{} {
	return []interface{}{n.Middlename, n.UserID}
}
func (n Middlename) DBUpdateArgs() []interface{} {
	return []interface{}{n.Middlename, n.MiddlenameID, n.UserID}
}
func (n Middlename) DBDeleteArgs() []interface{} {
	return []interface{}{n.MiddlenameID}
}
func (n *Middlename) DBSetUniqueID(a int) {
	n.MiddlenameID = a
}
