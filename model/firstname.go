package model

//Firstnames contain a list of them.
type Firstnames struct {
	Firstnames []Firstname `json:"firstnames"`
}

//Firstname exposes its string and corresponding owner.
type Firstname struct {
	FirstnameID int    `json:"firstname_id"`
	Firstname   string `json:"firstname"`
	UserID      int    `json:"user_id"`
}

//NewFirstname constructs an otherwise empty Firstname type pointer.
func NewFirstname() *Firstname {
	f := Firstname{0, "", 0}
	return &f
}

//NewFirstnameSlice constructs an otherwise empty Firstname slice pointer.
func NewFirstnameSlice() *Firstnames {
	f := Firstnames{}
	return &f
}

func (n Firstname) DBSelect() string {
	return "SELECT FIRSTNAME_ID, FIRSTNAME, USER_ID FROM FIRSTNAME WHERE USER_ID IN (?, ?)"
}
func (n Firstname) DBInsert() string {
	return "INSERT INTO FIRSTNAME (FIRSTNAME, USER_ID) VALUES (?, ?)"
}
func (n Firstname) DBUpdate() string {
	return "UPDATE FIRSTNAME SET FIRSTNAME = ? WHERE FIRSTNAME_ID = ? AND USER_ID = ?"
}
func (n Firstname) DBDelete() string {
	return "DELETE FROM FIRSTNAME WHERE FIRSTNAME_ID = ?"
}

func (n Firstname) DBQueryArgs() []interface{} {
	return []interface{}{1, n.UserID}
}
func (n Firstname) DBInsertArgs() []interface{} {
	return []interface{}{n.Firstname, n.UserID}
}
func (n Firstname) DBUpdateArgs() []interface{} {
	return []interface{}{n.Firstname, n.FirstnameID, n.UserID}
}
func (n Firstname) DBDeleteArgs() []interface{} {
	return []interface{}{n.FirstnameID}
}
func (n *Firstname) DBSetUniqueID(a int) {
	n.FirstnameID = a
}
