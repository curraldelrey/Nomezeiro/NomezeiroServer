package model

//Integration defines a registered integration value.
type Integration struct {
	IntegrationID int    `json:"integration_id"`
	Integration   string `json:"integration"`
	UserID        int    `json:"user_id"`
}

//Integrations contain a list of them.
type Integrations struct {
	Integrations []Integration `json:"integrations"`
}

//NewIntegration constructs an otherwise empty Integration type pointer.
func NewIntegration() *Integration {
	i := Integration{0, "", 0}
	return &i
}

//NewIntegrationSlice constructs an otherwise empty Integration slice pointer.
func NewIntegrationSlice() *Integrations {
	i := Integrations{}
	return &i
}

func (i Integration) DBSelect() string {
	return "SELECT INTEGRATION_ID, INTEGRATION, USER_ID FROM INTEGRATION WHERE USER_ID = ?"
}
func (i Integration) DBInsert() string {
	return "INSERT INTO INTEGRATION (INTEGRATION, USER_ID) VALUES (?, ?)"
}
func (i Integration) DBUpdate() string {
	return "UPDATE INTEGRATION SET INTEGRATION = ? WHERE INTEGRATION_ID = ? AND USER_ID = ?"
}
func (i Integration) DBDelete() string {
	return "DELETE FROM INTEGRATION WHERE INTEGRATION_ID = ?"
}

func (i Integration) DBQueryArgs() []interface{} {
	return []interface{}{i.UserID}
}
func (i Integration) DBInsertArgs() []interface{} {
	return []interface{}{i.Integration, i.UserID}
}
func (i Integration) DBUpdateArgs() []interface{} {
	return []interface{}{i.Integration, i.IntegrationID, i.UserID}
}
func (i Integration) DBDeleteArgs() []interface{} {
	return []interface{}{i.IntegrationID}
}
func (i *Integration) DBSetUniqueID(a int) {
	i.IntegrationID = a
}
