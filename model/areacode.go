package model

//Areacodes contain a list of them.
type Areacodes struct {
	Areacodes []Areacode `json:"areacodes"`
}

//Areacode exposes its string.
type Areacode struct {
	Areacode string `json:"areacode"`
}

//NewAreacode constructs an otherwise empty Areacode type pointer.
func NewAreacode() *Areacode {
	a := Areacode{""}
	return &a
}

//NewAreacodeSlice constructs an otherwise empty Areacode slice pointer.
func NewAreacodeSlice() *Areacodes {
	a := Areacodes{}
	return &a
}

func (ac Areacode) DBSelect() string {
	return "SELECT AREACODE FROM AREACODE"
}
func (ac Areacode) DBInsert() string {
	return ""
}
func (ac Areacode) DBUpdate() string {
	return ""
}
func (ac Areacode) DBDelete() string {
	return ""
}
func (ac Areacode) DBQueryArgs() []interface{} {
	return []interface{}{}
}
func (ac Areacode) DBSelectArgs() []interface{} {
	return []interface{}{}
}
func (ac Areacode) DBInsertArgs() []interface{} {
	return []interface{}{}
}
func (ac Areacode) DBUpdateArgs() []interface{} {
	return []interface{}{}
}
func (ac Areacode) DBDeleteArgs() []interface{} {
	return []interface{}{}
}
func (ac *Areacode) DBSetUniqueID(a int) {
	return
}
