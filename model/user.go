package model

//Credentials define the basic credentials for an user
type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

//User defines an actual, well, user.
type User struct {
	UserID    int    `json:"user_id"`
	Username  string `json:"username"`
	Timestamp int64  `json:"timestamp"`
}

//JwtToken defines the token an user can be identified with
type JwtToken struct {
	Token string `json:"token"`
	User  User   `json:"user"`
}

//NewUser constructs an otherwise empty User type pointer.
func NewUser() *User {
	u := User{0, "", 0}
	return &u
}
