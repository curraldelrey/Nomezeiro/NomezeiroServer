package model

//Phonenumber defines a registered phone.
type Phonenumber struct {
	PhoneID int    `json:"phone_id"`
	Phone   string `json:"phone"`
	UserID  int    `json:"user_id"`
}

//Phonenumbers contain a list of them.
type Phonenumbers struct {
	Phonenumbers []Phonenumber `json:"phonenumbers"`
}

//NewPhonenumber constructs an otherwise empty Phonenumber type pointer.
func NewPhonenumber() *Phonenumber {
	p := Phonenumber{0, "", 0}
	return &p
}

//NewPhonenumberSlice constructs an otherwise empty Phonenumber slice pointer.
func NewPhonenumberSlice() *Phonenumbers {
	p := Phonenumbers{}
	return &p
}

func (p Phonenumber) DBSelect() string {
	return "SELECT PHONE_ID, PHONE, USER_ID FROM PHONENUMBER WHERE USER_ID = ?"
}
func (p Phonenumber) DBInsert() string {
	return "INSERT INTO PHONENUMBER (PHONE, USER_ID) VALUES (?, ?)"
}
func (p Phonenumber) DBUpdate() string {
	return "UPDATE PHONENUMBER SET PHONE = ? WHERE PHONE_ID = ? AND USER_ID = ?"
}
func (p Phonenumber) DBDelete() string {
	return "DELETE FROM PHONENUMBER WHERE PHONE_ID = ?"
}
func (p Phonenumber) DBQueryArgs() []interface{} {
	return []interface{}{p.UserID}
}
func (p Phonenumber) DBInsertArgs() []interface{} {
	return []interface{}{p.Phone, p.UserID}
}
func (p Phonenumber) DBUpdateArgs() []interface{} {
	return []interface{}{p.Phone, p.PhoneID, p.UserID}
}
func (p Phonenumber) DBDeleteArgs() []interface{} {
	return []interface{}{p.PhoneID}
}
func (p *Phonenumber) DBSetUniqueID(a int) {
	p.PhoneID = a
}
