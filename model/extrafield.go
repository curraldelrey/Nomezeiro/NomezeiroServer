package model

//Extrafield defines a registered extrafield value.
type Extrafield struct {
	ExtrafieldID int    `json:"extrafield_id"`
	Fieldname    string `json:"fieldname"`
	Fieldvalue   string `json:"fieldvalue"`
	UserID       int    `json:"user_id"`
}

//Extrafields contain a list of them.
type Extrafields struct {
	Extrafields []Extrafield `json:"extrafields"`
}

//NewExtrafield constructs an otherwise empty Extrafield type pointer.
func NewExtrafield() *Extrafield {
	e := Extrafield{0, "", "", 0}
	return &e
}

//NewExtrafieldSlice constructs an otherwise empty Extrafield slice pointer.
func NewExtrafieldSlice() *Extrafields {
	e := Extrafields{}
	return &e
}

func (e Extrafield) DBSelect() string {
	return "SELECT EXTRAFIELD_ID, FIELDNAME, FIELDVALUE, USER_ID FROM EXTRAFIELD WHERE USER_ID = ?"
}
func (e Extrafield) DBInsert() string {
	return "INSERT INTO EXTRAFIELD (FIELDNAME, FIELDVALUE, USER_ID) VALUES (?, ?, ?)"
}
func (e Extrafield) DBUpdate() string {
	return "UPDATE EXTRAFIELD SET FIELDNAME = ?, FIELDVALUE = ? WHERE EXTRAFIELD_ID = ? AND USER_ID = ?"
}
func (e Extrafield) DBDelete() string {
	return "DELETE FROM EXTRAFIELD WHERE EXTRAFIELD_ID = ?"
}
func (e Extrafield) DBQueryArgs() []interface{} {
	return []interface{}{e.UserID}
}
func (e Extrafield) DBInsertArgs() []interface{} {
	return []interface{}{e.Fieldname, e.Fieldvalue, e.UserID}
}
func (e Extrafield) DBUpdateArgs() []interface{} {
	return []interface{}{e.Fieldname, e.Fieldvalue, e.ExtrafieldID, e.UserID}
}
func (e Extrafield) DBDeleteArgs() []interface{} {
	return []interface{}{e.ExtrafieldID}
}
func (e *Extrafield) DBSetUniqueID(a int) {
	e.ExtrafieldID = a
}
