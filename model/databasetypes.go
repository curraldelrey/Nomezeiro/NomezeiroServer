package model

//DBType defines an interface for types that interact with the database.
type DBType interface {
	DBSelect() string
	DBInsert() string
	DBUpdate() string
	DBDelete() string
	DBQueryArgs() []interface{}
	DBInsertArgs() []interface{}
	DBUpdateArgs() []interface{}
	DBDeleteArgs() []interface{}
	DBSetUniqueID(int)
}
