package model

//APIError describes an error to a REST API.
type APIError struct {
	Error string `json:"error"`
	Code  int    `json:"code"`
}
