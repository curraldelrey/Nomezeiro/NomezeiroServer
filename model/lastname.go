package model

//Lastnames contain a list om them.
type Lastnames struct {
	Lastnames []Lastname `json:"lastnames"`
}

//Lastname exposes its string and corresponding owner.
type Lastname struct {
	LastnameID int    `json:"lastname_id"`
	Lastname   string `json:"lastname"`
	UserID     int    `json:"user_id"`
}

//NewLastname constructs an otherwise empty Lastname type pointer.
func NewLastname() *Lastname {
	l := Lastname{0, "", 0}
	return &l
}

//NewLastnameSlice constructs an otherwise empty Lastname slice pointer.
func NewLastnameSlice() *Lastnames {
	l := Lastnames{}
	return &l
}

func (n Lastname) DBSelect() string {
	return "SELECT LASTNAME_ID, LASTNAME, USER_ID FROM LASTNAME WHERE USER_ID IN (?, ?)"
}
func (n Lastname) DBInsert() string {
	return "INSERT INTO LASTNAME (LASTNAME, USER_ID) VALUES (?, ?)"
}
func (n Lastname) DBUpdate() string {
	return "UPDATE LASTNAME SET LASTNAME = ? WHERE LASTNAME_ID = ? AND USER_ID = ?"
}
func (n Lastname) DBDelete() string {
	return "DELETE FROM LASTNAME WHERE LASTNAME_ID = ?"
}
func (n Lastname) DBQueryArgs() []interface{} {
	return []interface{}{1, n.UserID}
}
func (n Lastname) DBInsertArgs() []interface{} {
	return []interface{}{n.Lastname, n.UserID}
}
func (n Lastname) DBUpdateArgs() []interface{} {
	return []interface{}{n.Lastname, n.LastnameID, n.UserID}
}
func (n Lastname) DBDeleteArgs() []interface{} {
	return []interface{}{n.LastnameID}
}
func (n *Lastname) DBSetUniqueID(a int) {
	n.LastnameID = a
}
