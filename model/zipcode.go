package model

//Zipcode defines a registered zipcode valuz.
type Zipcode struct {
	ZipcodeID int    `json:"zipcode_id"`
	Zipcode   string `json:"zipcode"`
	UserID    int    `json:"user_id"`
}

//Zipcodes contain a list of them.
type Zipcodes struct {
	Zipcodes []Zipcode `json:"zipcodes"`
}

//NewZipcode constructs an otherwise empty Zipcode type pointer.
func NewZipcode() *Zipcode {
	z := Zipcode{0, "", 0}
	return &z
}

//NewZipcodeSlice constructs an otherwise empty Zipcode slice pointer.
func NewZipcodeSlice() *Zipcodes {
	z := Zipcodes{}
	return &z
}

func (z Zipcode) DBSelect() string {
	return "SELECT ZIPCODE_ID, ZIPCODE, USER_ID FROM ZIPCODE WHERE USER_ID = ?"
}
func (z Zipcode) DBInsert() string {
	return "INSERT INTO ZIPCODE (ZIPCODE, USER_ID) VALUES (?, ?)"
}
func (z Zipcode) DBUpdate() string {
	return "UPDATE ZIPCODE SET ZIPCODE = ? WHERE ZIPCODE_ID = ? AND USER_ID = ?"
}
func (z Zipcode) DBDelete() string {
	return "DELETE FROM ZIPCODE WHERE ZIPCODE_ID = ?"
}

func (z Zipcode) DBQueryArgs() []interface{} {
	return []interface{}{z.UserID}
}
func (z Zipcode) DBInsertArgs() []interface{} {
	return []interface{}{z.Zipcode, z.UserID}
}
func (z Zipcode) DBUpdateArgs() []interface{} {
	return []interface{}{z.Zipcode, z.ZipcodeID, z.UserID}
}
func (z Zipcode) DBDeleteArgs() []interface{} {
	return []interface{}{z.ZipcodeID}
}
func (z *Zipcode) DBSetUniqueID(a int) {
	z.ZipcodeID = a
}
