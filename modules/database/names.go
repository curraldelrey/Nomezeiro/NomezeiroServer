package database

import (
	"database/sql"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/model"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

//FindFirstnames returns a valid list of Firstnames or an error by using given credentials.
func FindFirstnames(v model.DBType) (*model.Firstnames, error) {
	var err error
	var stmt *sql.Stmt
	var res *sql.Rows
	n := model.NewFirstnameSlice()

	if stmt, err = dbPrepare(v.DBSelect()); err != nil {
		return n, err
	}

	if res, err = dbQuery(stmt, v.DBQueryArgs()); err != nil {
		return n, err
	}

	for res.Next() {
		var item model.Firstname
		if err := res.Scan(&item.FirstnameID, &item.Firstname, &item.UserID); err != nil {
			logging.Info("Error reading from the database: " + err.Error())
			return n, err
		}
		n.Firstnames = append(n.Firstnames, item)
	}
	logging.Trace("%v", n)
	return n, err
}

//FindMiddlenames returns a valid list of Middlenames or an error by using given credentials.
func FindMiddlenames(v model.DBType) (*model.Middlenames, error) {
	var err error
	var stmt *sql.Stmt
	var res *sql.Rows
	n := model.NewMiddlenameSlice()

	if stmt, err = dbPrepare(v.DBSelect()); err != nil {
		return n, err
	}

	if res, err = dbQuery(stmt, v.DBQueryArgs()); err != nil {
		return n, err
	}

	for res.Next() {
		var item model.Middlename
		if err := res.Scan(&item.MiddlenameID, &item.Middlename, &item.UserID); err != nil {
			logging.Info("Error reading from the database: " + err.Error())
			return n, err
		}
		n.Middlenames = append(n.Middlenames, item)
	}
	logging.Trace("%v", n)
	return n, err
}

//FindLastnames returns a valid list of Lastnames or an error by using given credentials.
func FindLastnames(v model.DBType) (*model.Lastnames, error) {
	var err error
	var stmt *sql.Stmt
	var res *sql.Rows
	n := model.NewLastnameSlice()

	if stmt, err = dbPrepare(v.DBSelect()); err != nil {
		return n, err
	}

	if res, err = dbQuery(stmt, v.DBQueryArgs()); err != nil {
		return n, err
	}

	for res.Next() {
		var item model.Lastname
		if err := res.Scan(&item.LastnameID, &item.Lastname, &item.UserID); err != nil {
			logging.Info("Error reading from the database: " + err.Error())
			return n, err
		}
		n.Lastnames = append(n.Lastnames, item)
	}
	logging.Trace("%v", n)
	return n, err
}
