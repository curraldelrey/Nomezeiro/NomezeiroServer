package database

import (
	"database/sql"
	"errors"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/model"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

func dbPrepare(query string) (*sql.Stmt, error) {
	stmt, err := DatabaseConnection.Prepare(query)
	if err != nil {
		logging.Critical("Error creating DB statement: " + err.Error())
	}
	return stmt, err
}

func dbExecute(stmt *sql.Stmt, vars []interface{}) (sql.Result, error) {
	res, err := stmt.Exec(vars...)
	if err != nil {
		logging.Critical("Error executing DB statement: " + err.Error())
	}
	return res, err
}

func dbQuery(stmt *sql.Stmt, vars []interface{}) (*sql.Rows, error) {
	res, err := stmt.Query(vars...)
	if err != nil {
		logging.Critical("Error reading the query: " + err.Error())
	}
	return res, err
}

//DefaultInsert inserts an element of a given type into the database.
func DefaultInsert(v model.DBType) error {
	var err error
	var stmt *sql.Stmt

	if stmt, err = dbPrepare(v.DBInsert()); err != nil {
		return err
	}

	var res sql.Result
	if res, err = dbExecute(stmt, v.DBInsertArgs()); err != nil {
		return err
	}
	id, _ := res.LastInsertId()
	v.DBSetUniqueID(int(id))

	return err
}

//DefaultUpdate updates an element of a given type into the database.
func DefaultUpdate(v model.DBType) error {
	var err error
	var stmt *sql.Stmt

	if stmt, err = dbPrepare(v.DBUpdate()); err != nil {
		return err
	}

	var res sql.Result
	if res, err = dbExecute(stmt, v.DBUpdateArgs()); err != nil {
		return err
	}
	affectedRows, _ := res.RowsAffected()
	if affectedRows == 0 {
		err = errors.New("No data was changed. The user may not have the rights to make the requested change")
	}

	return err
}

//DefaultDelete deletes an element of a given type into the database.
func DefaultDelete(v model.DBType) error {
	var err error
	var stmt *sql.Stmt

	if stmt, err = dbPrepare(v.DBDelete()); err != nil {
		return err
	}

	var res sql.Result
	if res, err = dbExecute(stmt, v.DBDeleteArgs()); err != nil {
		return err
	}
	affectedRows, _ := res.RowsAffected()
	if affectedRows == 0 {
		err = errors.New("No data was changed. The user may not have the rights to make the requested change")
	}

	return err
}
