package database

import (
	"database/sql"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/model"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

//FindAreacodes returns a valid list of Areacodes or an error.
func FindAreacodes(v model.DBType) (*model.Areacodes, error) {
	var err error
	var stmt *sql.Stmt
	var res *sql.Rows
	a := model.NewAreacodeSlice()

	if stmt, err = dbPrepare(v.DBSelect()); err != nil {
		return a, err
	}

	if res, err = dbQuery(stmt, v.DBQueryArgs()); err != nil {
		return a, err
	}

	for res.Next() {
		var item model.Areacode
		if err := res.Scan(&item.Areacode); err != nil {
			logging.Info("Error reading from the database: " + err.Error())
			return a, err
		}
		a.Areacodes = append(a.Areacodes, item)
	}
	logging.Trace("%v", a)
	return a, err
}
