package database

import (
	"database/sql"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/settings"

	//MySQL driver
	_ "github.com/go-sql-driver/mysql"
)

//DatabaseConnection represents the database connection pool that Go provides. Its scope is meant to be as long as possible.
var DatabaseConnection *sql.DB

//Initialize creates a MySQL database connection.
//The connectionString variable is created in the DSN format and a sql.DB* is opened.
//Internally this struct already works as a connection pool and is meant to be thread-safe.
func Initialize() error {
	logging.Info("Initializing database module")
	connectionString := settings.Settings.DatabaseData.Username + ":" +
		settings.Settings.DatabaseData.Password + "@tcp(" + settings.Settings.DatabaseData.Server + ":" +
		settings.Settings.DatabaseData.Port + ")/" + settings.Settings.DatabaseData.Schema
	logging.Info("Connecting with the database. ConnectionString: " + connectionString)
	var err error
	DatabaseConnection, err = sql.Open("mysql", connectionString)
	if err != nil {
		return err
	}
	logging.Info("Connection succeeded")
	if err = DatabaseConnection.Ping(); err != nil {
		return err
	}
	return nil
}

//TryThing performs a test ping in order to prove the pointer hasn't been lost.
func TryThing() bool {
	if err := DatabaseConnection.Ping(); err != nil {
		return false
	}
	return true
}

//Close terminates the connection slightly cleaner.
func Close() {
	DatabaseConnection.Close()
}
