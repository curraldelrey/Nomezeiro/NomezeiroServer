package database

import (
	"database/sql"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/model"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

//FindPhonenumbers returns a valid list of Phonenumbers or an error by using given credentials.
func FindPhonenumbers(v model.DBType) (*model.Phonenumbers, error) {
	var err error
	var stmt *sql.Stmt
	var res *sql.Rows
	p := model.NewPhonenumberSlice()
	if stmt, err = dbPrepare(v.DBSelect()); err != nil {
		return p, err
	}

	if res, err = dbQuery(stmt, v.DBQueryArgs()); err != nil {
		return p, err
	}

	for res.Next() {
		var item model.Phonenumber
		if err := res.Scan(&item.PhoneID, &item.Phone, &item.UserID); err != nil {
			logging.Info("Error reading from the database: " + err.Error())
			return p, err
		}
		p.Phonenumbers = append(p.Phonenumbers, item)
	}
	logging.Trace("%v", p)
	return p, err
}

//FindIntegrations returns a valid list of Integrations or an error by using given credentials.
func FindIntegrations(v model.DBType) (*model.Integrations, error) {
	var err error
	var stmt *sql.Stmt
	var res *sql.Rows
	i := model.NewIntegrationSlice()
	if stmt, err = dbPrepare(v.DBSelect()); err != nil {
		return i, err
	}

	if res, err = dbQuery(stmt, v.DBQueryArgs()); err != nil {
		return i, err
	}

	for res.Next() {
		var item model.Integration
		if err := res.Scan(&item.IntegrationID, &item.Integration, &item.UserID); err != nil {
			logging.Info("Error reading from the database: " + err.Error())
			return i, err
		}
		i.Integrations = append(i.Integrations, item)
	}
	logging.Trace("%v", i)
	return i, err
}

//FindZipcodes returns a valid list of Zipcodes or an error by using given credentials.
func FindZipcodes(v model.DBType) (*model.Zipcodes, error) {
	var err error
	var stmt *sql.Stmt
	var res *sql.Rows
	z := model.NewZipcodeSlice()

	if stmt, err = dbPrepare(v.DBSelect()); err != nil {
		return z, err
	}

	if res, err = dbQuery(stmt, v.DBQueryArgs()); err != nil {
		return z, err
	}

	for res.Next() {
		var item model.Zipcode
		if err := res.Scan(&item.ZipcodeID, &item.Zipcode, &item.UserID); err != nil {
			logging.Info("Error reading from the database: " + err.Error())
			return z, err
		}
		z.Zipcodes = append(z.Zipcodes, item)
	}
	logging.Trace("%v", z)
	return z, err
}

//FindExtrafields returns a valid list of Extrafields or an error by using given credentials.
func FindExtrafields(v model.DBType) (*model.Extrafields, error) {
	var err error
	var stmt *sql.Stmt
	var res *sql.Rows
	e := model.NewExtrafieldSlice()

	if stmt, err = dbPrepare(v.DBSelect()); err != nil {
		return e, err
	}

	if res, err = dbQuery(stmt, v.DBQueryArgs()); err != nil {
		return e, err
	}

	for res.Next() {
		var item model.Extrafield
		if err := res.Scan(&item.ExtrafieldID, &item.Fieldname, &item.Fieldvalue, &item.UserID); err != nil {
			logging.Info("Error reading from the database: " + err.Error())
			return e, err
		}
		e.Extrafields = append(e.Extrafields, item)
	}
	logging.Trace("%v", e)
	return e, err
}
