package database

import (
	"bytes"
	"strings"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/model"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/crypto"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/util"
)

//DBFunc describes the signature for database functions.
type DBFunc func(model.DBType) error

type DBValueType []interface{}

type PopulateType int

type Populator struct {
	query     string
	argument  string
	filename  string
	finaltype PopulateType
}

const (
	PTypeFirst  PopulateType = 0
	PTypeMiddle PopulateType = 1
	PTypeLast   PopulateType = 2
	PTypeArea   PopulateType = 3
)

var (
	PopFirstname = Populator{
		"INSERT INTO FIRSTNAME (FIRSTNAME, USER_ID) VALUES ",
		"(?, ?),",
		"firstnames.json",
		PTypeFirst,
	}
	PopMiddlename = Populator{
		"INSERT INTO MIDDLENAME (MIDDLENAME, USER_ID) VALUES ",
		"(?, ?),",
		"middlenames.json",
		PTypeMiddle,
	}
	PopLastname = Populator{
		"INSERT INTO LASTNAME (LASTNAME, USER_ID) VALUES ",
		"(?, ?),",
		"lastnames.json",
		PTypeLast,
	}
	PopAreacode = Populator{
		"INSERT INTO AREACODE (AREACODE) VALUES ",
		"(?),",
		"areacodes.json",
		PTypeArea,
	}
)

//CreateInitialConfig initializes the database tables.
func CreateInitialConfig() bool {
	var err error

	logging.Info("Creating new database.")
	statements := []string{
		"DROP TABLE IF EXISTS FIRSTNAME,MIDDLENAME,LASTNAME,PHONENUMBER,AREACODE,INTEGRATION,ZIPCODE,EXTRAFIELD,USER",
		"CREATE TABLE IF NOT EXISTS USER (USER_ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, USERNAME VARCHAR(30) NOT NULL UNIQUE, PASSWORD VARCHAR(64) NOT NULL, PRIMARY KEY (USER_ID))",
		"CREATE TABLE IF NOT EXISTS FIRSTNAME (FIRSTNAME_ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, FIRSTNAME VARCHAR(64) NOT NULL, USER_ID INT(10) UNSIGNED NOT NULL, PRIMARY KEY (FIRSTNAME_ID), FOREIGN KEY (USER_ID) REFERENCES USER(USER_ID), CONSTRAINT UID_FN UNIQUE (USER_ID, FIRSTNAME))",
		"CREATE TABLE IF NOT EXISTS MIDDLENAME (MIDDLENAME_ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, MIDDLENAME VARCHAR(64) NOT NULL, USER_ID INT(10) UNSIGNED NOT NULL, PRIMARY KEY (MIDDLENAME_ID),  FOREIGN KEY (USER_ID) REFERENCES USER(USER_ID), CONSTRAINT UID_MN UNIQUE (USER_ID, MIDDLENAME))",
		"CREATE TABLE IF NOT EXISTS LASTNAME (LASTNAME_ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, LASTNAME VARCHAR(64) NOT NULL, USER_ID INT(10) UNSIGNED NOT NULL, PRIMARY KEY (LASTNAME_ID), FOREIGN KEY (USER_ID) REFERENCES USER(USER_ID), CONSTRAINT UID_LN UNIQUE (USER_ID, LASTNAME))",
		"CREATE TABLE IF NOT EXISTS PHONENUMBER (PHONE_ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, PHONE VARCHAR(16) NOT NULL, USER_ID INT(10) UNSIGNED NOT NULL, PRIMARY KEY (PHONE_ID), FOREIGN KEY (USER_ID) REFERENCES USER(USER_ID))",
		"CREATE TABLE IF NOT EXISTS AREACODE (AREACODE VARCHAR(8) NOT NULL)",
		"CREATE TABLE IF NOT EXISTS INTEGRATION (INTEGRATION_ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, INTEGRATION VARCHAR(50) NOT NULL, USER_ID INT(10) UNSIGNED NOT NULL, PRIMARY KEY (INTEGRATION_ID), FOREIGN KEY (USER_ID) REFERENCES USER(USER_ID))",
		"CREATE TABLE IF NOT EXISTS ZIPCODE (ZIPCODE_ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, ZIPCODE VARCHAR(50) NOT NULL, USER_ID INT(10) UNSIGNED NOT NULL, PRIMARY KEY (ZIPCODE_ID), FOREIGN KEY (USER_ID) REFERENCES USER(USER_ID))",
		"CREATE TABLE IF NOT EXISTS EXTRAFIELD (EXTRAFIELD_ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, FIELDNAME VARCHAR(50) NOT NULL, FIELDVALUE VARCHAR(100) NOT NULL, USER_ID INT(10) UNSIGNED NOT NULL, PRIMARY KEY (EXTRAFIELD_ID), FOREIGN KEY (USER_ID) REFERENCES USER(USER_ID))",
		"INSERT INTO USER (USERNAME, PASSWORD) VALUES ('DEFAULT','" + crypto.HashAndSalt([]byte("DEFAULT")) + "')",
	}

	logging.Trace("Running table statements.")
	for _, stmt := range statements {
		statement, err := DatabaseConnection.Prepare(stmt)
		if err != nil {
			logging.Critical("Error creating DB statement: " + err.Error())
			return false
		}
		_, execerr := statement.Exec()
		if execerr != nil {
			logging.Critical("Error executing DB statement: " + err.Error())
			return false
		}
	}

	logging.Trace("Database skeleton created.")
	logging.Trace("Populating tables with default data.")

	err = populateTable(PopFirstname)
	err = populateTable(PopMiddlename)
	err = populateTable(PopLastname)
	err = populateTable(PopAreacode)

	if err != nil {
		return false
	}
	logging.Info("Database created and ready.")
	return true
}

func populateTable(t Populator) error {
	var buf bytes.Buffer
	vals := DBValueType{}
	var err error
	buf.WriteString(t.query)

	vals = createArgs(t, &buf)
	query := strings.TrimSuffix(buf.String(), ",")
	stmt, _ := DatabaseConnection.Prepare(query)
	_, err = stmt.Exec(vals...)

	if err != nil {
		logging.Critical("Error populating the database: " + err.Error())
	}
	return err
}

func createArgs(t Populator, buf *bytes.Buffer) DBValueType {
	vals := DBValueType{}
	switch t.finaltype {
	case PTypeFirst:
		var l model.Firstnames
		util.LoadJSONFromFile(t.filename, &l)
		for _, row := range l.Firstnames {
			buf.WriteString(t.argument)
			vals = append(vals, row.Firstname, row.UserID)
		}
		break
	case PTypeMiddle:
		var l model.Middlenames
		util.LoadJSONFromFile(t.filename, &l)
		for _, row := range l.Middlenames {
			buf.WriteString(t.argument)
			vals = append(vals, row.Middlename, row.UserID)
		}
		break
	case PTypeLast:
		var l model.Lastnames
		util.LoadJSONFromFile(t.filename, &l)
		for _, row := range l.Lastnames {
			buf.WriteString(t.argument)
			vals = append(vals, row.Lastname, row.UserID)
		}
		break
	case PTypeArea:
		var a model.Areacodes
		util.LoadJSONFromFile(t.filename, &a)
		for _, row := range a.Areacodes {
			buf.WriteString(t.argument)
			vals = append(vals, row.Areacode)
		}
		break
	}
	return vals

}
