package database

import (
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/model"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/crypto"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

//FindUser returns a valid User or an error by using given credentials.
func FindUser(user string) (*model.User, error) {
	var err error
	u := model.NewUser()

	stmt, err := DatabaseConnection.Prepare("SELECT USER_ID, USERNAME FROM USER WHERE USERNAME = ?")
	if err != nil {
		logging.Critical("Error creating db statement: " + err.Error())
		return u, err
	}

	res, err := stmt.Query(user)
	if err != nil {
		logging.Critical("Error reading the query: " + err.Error())
		return u, err
	}

	if res.Next() == false {
		logging.Trace("No results found in the database for the given credentials.")
		return u, err
	}
	if err := res.Scan(&u.UserID, &u.Username); err != nil {
		logging.Info("Error reading from the database: " + err.Error())
		return u, err
	}
	return u, err
}

//GetUserPassword returns the hashed password for verification purposes.
func GetUserPassword(id int) ([]byte, error) {
	var err error
	var p string

	stmt, err := DatabaseConnection.Prepare("SELECT PASSWORD FROM USER WHERE USER_ID = ?")
	if err != nil {
		logging.Critical("Error creating db statement: " + err.Error())
		return []byte(p), err
	}

	res, err := stmt.Query(id)
	if err != nil {
		logging.Critical("Error reading the query: " + err.Error())
		return []byte(p), err
	}

	if res.Next() == false {
		logging.Trace("No results found in the database for the given credentials.")
		return []byte(p), err
	}
	if err := res.Scan(&p); err != nil {
		logging.Info("Error reading from the database: " + err.Error())
		return []byte(p), err
	}
	return []byte(p), err
}

//AddUser creates a new user in the database.
func AddUser(c *model.Credentials) error {
	var err error
	query := "INSERT INTO USER (USERNAME, PASSWORD) VALUES (?, ?)"

	stmt, err := DatabaseConnection.Prepare(query)
	if err != nil {
		logging.Critical("Error creating DB statement: " + err.Error())
		return err
	}

	_, err = stmt.Exec(c.Username, crypto.HashAndSalt([]byte(c.Password)))
	if err != nil {
		logging.Critical("Error executing DB statement: " + err.Error())
		return err
	}

	return err
}
