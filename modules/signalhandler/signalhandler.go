package signalhandler

import (
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

var (
	//Close is a public channel that informs the main module that the application should be closed.
	Close   chan bool
	sigchan chan os.Signal
)

//Initialize prepares the signal handler channel and sends it to the main loop.
func Initialize() error {
	Close = make(chan bool, 1)
	sigchan = make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)
	logging.Info("Signal handler initalized.")

	go signalHandler()

	return nil
}

func signalHandler() {
	<-sigchan
	logging.Info("Terminate signal called. Closing application.")
	Close <- true
}
