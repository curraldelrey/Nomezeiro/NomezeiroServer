package daemon

import (
	"log"
	"os"
	"strings"
)

//RunCommand parses a command and sends it to the responsible module.
func RunCommand(command []string) {
	if command[0] == "help" {
		listCommands()
		os.Exit(0)
	}
	pipe, err := os.OpenFile(pipefile, os.O_WRONLY, 0600)
	if err != nil {
		if err.Error() == "open .nomezeiropipe: no such file or directory" {
			log.Fatal("NomezeiroServer seems to not be running or in a failed state. Command failed.")
		} else {
			log.Fatal("Error: " + err.Error())
		}
	}
	defer pipe.Close()

	pipe.Write([]byte(strings.Join(command, " ")))
	pipe.Write([]byte("\n"))

	log.Fatal("Command sent to the daemon.")
}

func listCommands() {
	log.Println("Available commands:")
	log.Println("\"server\": initializes the daemon. There must be one running to send commands.")
	log.Println("\"keys\": Creates a keypair for crypto. This must be run once before initializing the database. Running it again will require a database re-creation for now.")
	log.Println("\"init\": Creates initial structures and data in the configured database. This must be run at least once.")
	log.Println("\"hash\" <arg1>: Tests crypto by hashing <arg1> and presenting the result.")
	log.Println("\"newuser\" <arg1> <arg2>: Creates a new user with the given credentials. The password is encrypted before being sent to the database.")
}
