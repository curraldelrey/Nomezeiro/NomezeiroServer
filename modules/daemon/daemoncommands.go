package daemon

import (
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/model"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/database"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

func createUser(args []string) {
	logging.Info("Creating user " + args[0] + "...")
	c := model.Credentials{Username: args[0], Password: args[1]}

	err := database.AddUser(&c)
	if err != nil {
		logging.Critical("Error adding new user: " + err.Error())
		return
	}
	logging.Info("User created successfully.")
}
