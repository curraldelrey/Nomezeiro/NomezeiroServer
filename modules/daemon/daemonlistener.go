package daemon

import (
	"bufio"
	"os"
	"strings"
	"syscall"
	"time"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/crypto"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/database"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

var (
	reader   *bufio.Reader
	pipefile = ".nomezeiropipe"
)

//Initialize prepares the daemon and sends it to the main loop.
func Initialize() {
	logging.Info("Initializing pipe")
	os.Remove(pipefile)
	syscall.Mkfifo(pipefile, 0600)

	pipe, err := os.OpenFile(pipefile, os.O_RDWR, os.ModeNamedPipe)
	if err != nil {
		logging.Critical("Error loading pipe. Error: " + err.Error())
	}
	logging.Info("Pipe initialized.")
	reader = bufio.NewReader(pipe)
	logging.Info("Daemon initialized.")
	go daemonLoop()
}

//daemonLoop runs indefinitely, looking for messages to parse.
func daemonLoop() {
	for {
		line, err := reader.ReadBytes('\n')
		if err != nil {
			logging.Info("Error: " + err.Error())
			break
		}
		strline := strings.TrimSuffix(string(line), "\n")
		if len(strline) != 0 {
			logging.Info("Pipe received. Content: " + strline)
			go parseCommands(strings.Split(strline, " "))
		}
		time.Sleep(100 * time.Millisecond)
	}
}

func parseCommands(command []string) {
	switch command[0] {
	case "init":
		go database.CreateInitialConfig()
	case "keys":
		go crypto.GenerateKeyPair()
	case "hash":
		go crypto.HashThis(command[1])
	case "newuser":
		go createUser(command[1:])
	default:
		logging.Info("Unknown command issued. Command: " + command[0])
	}
}

//Close performs additional clean-up.
func Close() {
	os.Remove(pipefile)
}
