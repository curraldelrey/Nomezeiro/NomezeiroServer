package util

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

//LoadJSONFromFile loads a serializable JSON type into a value's memory.
func LoadJSONFromFile(file string, values interface{}) {
	jsonFile, err := os.Open("res/" + file)
	if err != nil {
		if logging.IsInitialized {
			logging.Critical("Error opening a JSON file. Error: " + err.Error())
		} else {
			log.Fatal("Error opening a JSON file. Error: " + err.Error())
		}
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &values)
}
