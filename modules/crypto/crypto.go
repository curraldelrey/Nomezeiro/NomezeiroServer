package crypto

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/gob"
	"encoding/pem"
	"io/ioutil"
	"log"
	"os"

	Rsa "github.com/dvsekhvalnov/jose2go/keys/rsa"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
	"golang.org/x/crypto/bcrypt"
)

var (
	prvKey *rsa.PrivateKey
	pubKey *rsa.PublicKey
)

//HashAndSalt hashes and salts a given byte array, returning a string.
func HashAndSalt(password []byte) string {
	hash, err := bcrypt.GenerateFromPassword(password, 10)
	if err != nil {
		log.Println("err")
	}

	return string(hash)
}

//CompareHash confirms whether the password is correct.
func CompareHash(hash []byte, password []byte) bool {
	err := bcrypt.CompareHashAndPassword(hash, password)
	if err != nil {
		return false
	}
	return true
}

//GenerateKeyPair creates a RSA keypair for use within the server.
func GenerateKeyPair() {
	logging.Trace("Generating RSA-4096 keypair...")
	randomReader := rand.Reader
	bitSize := 4096

	key, err := rsa.GenerateKey(randomReader, bitSize)
	if err != nil {
		logging.Critical("Error generating the RSA keypair: " + err.Error())
	}

	os.Mkdir("keys", 0777)

	publicKey := key.PublicKey

	logging.Trace("RSA-4096 keypair generated. Saving private key...")

	saveGobKey("private.key", key)
	savePrivatePEMKey("private.pem", key)

	logging.Trace("Saving public key...")

	saveGobKey("public.key", publicKey)
	savePublicPEMKey("public.pem", publicKey)

	logging.Info("Keypair publishing complete.")
}

//saveGobKey saves the original binary blobs that comprise the keys.
func saveGobKey(fileName string, key interface{}) {
	outFile, err := os.Create("keys/" + fileName)
	if err != nil {
		logging.Critical("Error creating a RSA keypair file: " + err.Error())
	}
	defer outFile.Close()

	encoder := gob.NewEncoder(outFile)
	err = encoder.Encode(key)
	if err != nil {
		logging.Critical("Error saving a RSA keypair file: " + err.Error())
	}

}

//savePrivatePEMKey outputs a readable version of the private key.
func savePrivatePEMKey(fileName string, key *rsa.PrivateKey) {
	outFile, err := os.Create("keys/" + fileName)
	if err != nil {
		logging.Critical("Error creating a RSA private key PEM file: " + err.Error())
	}
	defer outFile.Close()

	var privateKey = &pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(key),
	}

	err = pem.Encode(outFile, privateKey)
	if err != nil {
		logging.Critical("Error saving a RSA private key PEM file: " + err.Error())
	}
}

//savePublicPEMKey outputs a readable version of the public key.
func savePublicPEMKey(fileName string, pubKey rsa.PublicKey) {
	asn1Bytes, err := x509.MarshalPKIXPublicKey(&pubKey)
	if err != nil {
		logging.Critical("Error marshalling a RSA public key PEM file: " + err.Error())
	}

	var pemkey = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: asn1Bytes,
	}

	pemfile, err := os.Create("keys/" + fileName)
	if err != nil {
		logging.Critical("Error creating a RSA public key PEM file: " + err.Error())
	}
	defer pemfile.Close()

	err = pem.Encode(pemfile, pemkey)
	if err != nil {
		logging.Critical("Error saving a RSA public key PEM file: " + err.Error())
	}
}

//Initialize starts the non-static elements of the crypto module.
func Initialize() error {
	logging.Trace("Initializing crypto module")

	prvBytes, _ := ioutil.ReadFile("keys/private.pem")
	var err error

	prvKey, err = Rsa.ReadPrivate(prvBytes)
	if err != nil {
		logging.Critical("Could not initialize the private key. Private key set as null.")
		prvKey = nil
	}

	pubBytes, _ := ioutil.ReadFile("keys/public.pem")

	pubKey, err = Rsa.ReadPublic(pubBytes)
	if err != nil {
		logging.Critical("Could not initialize the public key. Public key set as null.")
		pubKey = nil
	}

	return nil
}

//HashThis is a debug function.
func HashThis(v string) {
	logging.Info("Hash: " + HashAndSalt([]byte(v)))
}

//GetPublicKey retrieves the RSA public key.
func GetPublicKey() interface{} {
	return pubKey
}

//GetPrivateKey retrieves the RSA private key.
func GetPrivateKey() interface{} {
	return prvKey
}
