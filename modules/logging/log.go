package logging

import (
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

var (
	traceLogger    *log.Logger
	infoLogger     *log.Logger
	criticalLogger *log.Logger
	//IsInitialized Determines if the module can be used.
	IsInitialized bool
)

//Initialize instantiates all logging interfaces.
func Initialize() error {
	log.Println("Initializing logging module.")
	logfile := "logs/log-" + time.Now().Local().Format("2006-01-02_15:04:05")
	os.Mkdir("logs", 0777)

	file, err := os.OpenFile(logfile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	commonHandler := io.MultiWriter(file, os.Stdout)
	criticalHandler := io.MultiWriter(file, os.Stdout, os.Stderr)
	traceLogger = log.New(commonHandler, "Trace: ", log.Ldate|log.Ltime|log.Lshortfile)
	infoLogger = log.New(commonHandler, "Info: ", log.Ldate|log.Ltime|log.Lshortfile)
	criticalLogger = log.New(criticalHandler, "Critical: ", log.Ldate|log.Ltime|log.Lshortfile)

	IsInitialized = true
	return nil
}

//Trace defines trace-level logging. Fairly internal debugging stuff.
func Trace(pattern string, args ...interface{}) {
	traceLogger.Output(2, fmt.Sprintf(pattern, args...))
}

//Info defines general procedures in the application. Normal use? Use this.
func Info(pattern string, args ...interface{}) {
	infoLogger.Output(2, fmt.Sprintf(pattern, args...))
}

//Critical defines actual situations that should be able to crash. Assume a crash for now.
func Critical(pattern string, args ...interface{}) {
	criticalLogger.Output(2, fmt.Sprintf(pattern, args...))
}
