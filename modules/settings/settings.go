package settings

import (
	"os"
	"path/filepath"

	"github.com/BurntSushi/toml"
	"gitlab.com/curraldelrey/Nomezeiro/NomezeiroServer/modules/logging"
)

//DatabaseData holds information pertaining to its section in the settings file.
type DatabaseData struct {
	Server   string
	Port     string
	Schema   string
	Username string
	Password string
}

//Config is the main type that holds the information acquired from the settings file.
// type Config struct {
// 	title    string
// 	database DatabaseData
// }
type Config struct {
	DatabaseData DatabaseData `toml:"database"`
}

//Settings serves as a container for the configuration once loaded.
var Settings Config

//Initialize populates the Config struct with the settings set in file.
func Initialize() error {
	logging.Info("Initializing settings module")
	ex, err := os.Executable()
	if err != nil {
		return err
	}
	if _, err := toml.DecodeFile(filepath.Dir(ex)+"/settings.toml", &Settings); err != nil {
		return err
	}
	return nil
}
